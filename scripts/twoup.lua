-- name: Two-Up Alley
-- author: Andrew Pamment
-- version: 0.1

math.randomseed(os.time());

fh_print("`bright white`Two-up Alley`white`\r\n");
fh_print("---------------------------------------------------------\r\n");
fh_print("A seedy looking gentleman greets you,\r\n");
fh_print("         \"Care for a game of Two-Up?\"\r\n\r\n");

fh_print("---------------------------------------------------------\r\n");
local gold = fh_get_gold();
fh_print("You have `bright yellow`" .. math.floor(gold) .. "`white` gold. (Y / N) ? ");

local inp = fh_getkey();

if inp == "y" or inp == "Y" then
    local fin = 0;

    fh_print("\r\n\r\nAlright then, how much do you wager? ");

    local wager = tonumber(fh_getstring(5))

    if wager < 1 then
        fh_print("\r\n\r\nCome back when you're willing to bet at least 1 gold\r\n")
    elseif wager > gold then
        fh_print("\r\n\r\nTrying to scam me?? You don't have that much gold.\r\n")
    else
        while(fin ~= 1) do
            fh_print("To flip the coins, press ENTER....\r\n");
            fh_getkey();

            local coin1 = math.random(2);
            local coin2 = math.random(2);

            fh_print("You flipped ")
            if (coin1 == 1) then
                fh_print("`red`Tails`white` and ")
            else
                fh_print("`green`Heads`white` and ")
            end

            if (coin2 == 1) then
                fh_print("`red`Tails`white` ")
            else
                fh_print("`green`Heads`white` ")
            end

            if coin1 ~= coin2 then
                fh_print("... flip again!\r\n\r\n");
            else 
                if coin1 == 1 then
                    gold = gold - wager
                    fh_print("... You lose!")
                else 
                    gold = gold + wager
                    fh_print("... You Win!")
                end
                fh_set_gold(gold)
                fin = 1
            end
        end
    end

    fh_print("\r\n\r\nPress ENTER to return to town.\r\n");
    fh_getkey();
end
