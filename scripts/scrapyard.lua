-- name: Rico's Scrapyard
-- author: Andrew Pamment
-- version: 0.1

fh_print("`bright white`Rico's Scrapyard`white`\r\n");
fh_print("---------------------------------------------------------\r\n");
while true do
    fh_print("Rico looks you over, want to sell something?\r\n\r\n");

    local aval, astr = fh_get_armor();
    local wval, wstr = fh_get_weapon();

    if aval > 0 then
        fh_print(" (A) Sell your " .. astr .. "\r\n");
    end
    if wval > 0 then
        fh_print(" (W) Sell your " .. wstr .. "\r\n");
    end

    if aval == 0 and wval == 0 then
        fh_print(" You have nothing to sell!\r\n");
    end

    fh_print("\r\n (R) Return to Town\r\n\r\n");

    local inp = fh_getkey();

    if inp == 'a' or inp == 'A' then
        if (aval == 0) then
            fh_print("\r\nHuh?!\r\n");
        else
            fh_print("I'll give you " .. math.floor(aval * 1000 / 3) .. " for that junky armor. (Y/ N) ? ");
            local yn = fh_getkey();
            if yn == 'y' or yn == 'Y' then
                fh_set_armor(0);
                local gold = fh_get_gold();
                gold = gold + math.floor(aval * 1000 / 3);
                fh_set_gold(gold);
                fh_print("\r\n\r\nDone! You feel a little chilly.\r\n\r\n");
            end
        end
    elseif inp == 'w' or inp == 'W' then
        if (wval == 0) then
            fh_print("\r\nHuh?!\r\n");
        else
            fh_print("I'll give you " .. math.floor(wval * 1000 / 3) .. " for that junky weapon. (Y/ N) ? ");
            local yn = fh_getkey();
            if yn == 'y' or yn == 'Y' then
                fh_set_weapon(0);
                local gold = fh_get_gold();
                gold = gold + math.floor(wval * 1000 / 3);
                fh_set_gold(gold);
                fh_print("\r\n\r\nDone! You feel a little vulnerable.\r\n\r\n");
            end
        end
    else
        return
    end
end