#include "forhonour.h"
#include <OpenDoor.h>
#include <ctype.h>
#include <dirent.h>
#include <lauxlib.h>
#include <limits.h>
#include <lua.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef WIN32
#define _MSC_VER 1
#endif // WIN32

#ifdef _MSC_VER
#define PATH_SEP '\\'
#define PATH_MAX MAX_PATH
#else
#define PATH_SEP '/'
#endif

extern struct user_info info;

struct otherplaces_t {
  char *luascript;
  char *name;
  char *author;
  char *version;
};

static struct otherplaces_t **otherplaces;
static int otherplaces_count = 0;

void chomp(char *str) {
  char *end;
  size_t len = strlen(str);
  if (len == 0) {
    return;
  }
  end = str + len - 1;
  while (end != str && (*end == '\r' || *end == '\n' || *end == ' ')) {
    *end-- = '\0';
  }
}

int load_other_places() {
  DIR *dirp;
  FILE *fptr;
  char buffer[PATH_MAX];
  struct dirent *dent;
  char *name;
  char *author;
  char *version;
  dirp = opendir("scripts");
  if (dirp) {
    while ((dent = readdir(dirp))) {
      if (strcasecmp(&dent->d_name[strlen(dent->d_name) - 4], ".lua") == 0) {
        snprintf(buffer, PATH_MAX, "scripts%c%s", PATH_SEP, dent->d_name);
        fptr = fopen(buffer, "r");
        if (fptr) {
          name = NULL;
          author = NULL;
          version = NULL;
          fgets(buffer, PATH_MAX, fptr);
          chomp(buffer);
          if (strncasecmp(buffer, "-- name: ", 9) == 0) {
            name = strdup(&buffer[9]);
          }
          fgets(buffer, PATH_MAX, fptr);
          chomp(buffer);
          if (strncasecmp(buffer, "-- author: ", 11) == 0) {
            author = strdup(&buffer[11]);
          }

          fgets(buffer, PATH_MAX, fptr);
          chomp(buffer);
          if (strncasecmp(buffer, "-- version: ", 12) == 0) {
            version = strdup(&buffer[12]);
          }
          fclose(fptr);

          if (name == NULL) {
            name = strdup("Somewhere mysterious...");
          }

          if (otherplaces_count == 0) {
            otherplaces =
                (struct otherplaces_t **)malloc(sizeof(struct otherplaces_t *));
          } else {
            otherplaces = (struct otherplaces_t **)realloc(
                otherplaces,
                sizeof(struct otherplaces_t *) * (otherplaces_count + 1));
          }
          otherplaces[otherplaces_count] =
              (struct otherplaces_t *)malloc(sizeof(struct otherplaces_t));
          otherplaces[otherplaces_count]->luascript = strdup(dent->d_name);
          otherplaces[otherplaces_count]->name = name;
          otherplaces[otherplaces_count]->author = author;
          otherplaces[otherplaces_count]->version = version;
          otherplaces_count++;
        }
      }
    }
    closedir(dirp);
  }

  return otherplaces_count;
}

int lua_getGold(lua_State *L) {
  lua_pushnumber(L, info.gold);
  return 1;
}

int lua_setGold(lua_State *L) {
  info.gold = lua_tonumber(L, -1);
  save_player();
  return 0;
}

int lua_getLevel(lua_State *L) {
  lua_pushnumber(L, info.level);
  return 1;
}

int lua_getUserClass(lua_State *L) {
  lua_pushnumber(L, info.userClass);
  return 1;
}

int lua_getLuck(lua_State *L) {
  lua_pushnumber(L, info.luck);
  return 1;
}

int lua_setLuck(lua_State *L) {
  info.luck = lua_tonumber(L, -1);
  save_player();
  return 0;
}

int lua_getCharm(lua_State *L) {
  lua_pushnumber(L, info.charm);
  return 1;
}

int lua_setCharm(lua_State *L) {
  info.charm = lua_tonumber(L, -1);
  save_player();
  return 0;
}

int lua_getGems(lua_State *L) {
  lua_pushnumber(L, info.gems);
  return 1;
}

int lua_setGems(lua_State *L) {
  info.gems = lua_tonumber(L, -1);
  save_player();
  return 0;
}

int lua_setArmor(lua_State *L) {
  info.armorvalue = lua_tonumber(L, -1);
  save_player();
  return 0;
}

int lua_setWeapon(lua_State *L) {
  info.weaponvalue = lua_tonumber(L, -1);
  save_player();
  return 0;
}

int lua_print(lua_State *L) {
  const char *str = lua_tostring(L, -1);
  char *str2;
  char *ptr = (char *)str;
  int len = 0;

  while (*ptr) {
    if (*ptr == '%')
      len += 2;
    else
      len++;
    ptr++;
  }

  len++;

  str2 = (char *)malloc(len);
  if (!str2) {
    return 0;
  }

  ptr = str;
  len = 0;
  while (*ptr) {
    if (*ptr == '%') {
      str2[len++] = '%';
      str2[len++] = '%';
    } else {
      str2[len++] = *ptr;
    }
    ptr++;
  }
  str2[len] = '\0';

  od_printf(str2);

  free(str2);
  return 0;
}

static char last_key = 'x';

int lua_getKey(lua_State *L) {
  char c;

  c = od_get_key(TRUE);
  if ((c == '\n' || c == '\0') && last_key == '\r') {
    c = od_get_key(TRUE);
  }
  last_key = c;

  lua_pushlstring(L, &c, 1);

  return 1;
}

int lua_getWeapon(lua_State *L) {
  lua_pushnumber(L, info.weaponvalue);
  lua_pushstring(L, get_weapon_name());

  return 2;
}

int lua_getArmor(lua_State *L) {
  lua_pushnumber(L, info.armorvalue);
  lua_pushstring(L, get_armor_name());
  return 2;
}

int lua_getString(lua_State *L) {
  char buffer[256];
  int len = lua_tonumber(L, -1);

  if (len > 256) {
    len = 256;
  }

  od_input_str(buffer, len, 32, 126);

  lua_pushstring(L, buffer);

  return 1;
}

int lua_addNewsItem(lua_State *L) {
  const char *str = lua_tostring(L, -1);
  add_news_item((char *)str);
  return 0;
}

int lua_sendFile(lua_State *L) {
  const char *str = lua_tostring(L, -1);
  char buffer[PATH_MAX];
  snprintf(buffer, sizeof buffer, "scripts%c%s", PATH_SEP, str);
  od_send_file(buffer);
  return 0;
}

int lua_clrScreen(lua_State *L) {
  od_clr_scr();
  return 0;
}

void lua_push_cfunctions(lua_State *L) {
  lua_pushcfunction(L, lua_setGold);
  lua_setglobal(L, "fh_set_gold");
  lua_pushcfunction(L, lua_getGold);
  lua_setglobal(L, "fh_get_gold");
  lua_pushcfunction(L, lua_print);
  lua_setglobal(L, "fh_print");
  lua_pushcfunction(L, lua_getKey);
  lua_setglobal(L, "fh_getkey");
  lua_pushcfunction(L, lua_getString);
  lua_setglobal(L, "fh_getstring");
  lua_pushcfunction(L, lua_getUserClass);
  lua_setglobal(L, "fh_get_userclass");
  lua_pushcfunction(L, lua_getLevel);
  lua_setglobal(L, "fh_get_level");
  lua_pushcfunction(L, lua_setLuck);
  lua_setglobal(L, "fh_set_luck");
  lua_pushcfunction(L, lua_getLuck);
  lua_setglobal(L, "fh_get_luck");
  lua_pushcfunction(L, lua_setCharm);
  lua_setglobal(L, "fh_set_charm");
  lua_pushcfunction(L, lua_getCharm);
  lua_setglobal(L, "fh_get_charm");
  lua_pushcfunction(L, lua_setGems);
  lua_setglobal(L, "fh_set_gems");
  lua_pushcfunction(L, lua_getGems);
  lua_setglobal(L, "fh_get_gems");
  lua_pushcfunction(L, lua_addNewsItem);
  lua_setglobal(L, "fh_addnewsitem");
  lua_pushcfunction(L, lua_getArmor);
  lua_setglobal(L, "fh_get_armor");
  lua_pushcfunction(L, lua_getWeapon);
  lua_setglobal(L, "fh_get_weapon");
  lua_pushcfunction(L, lua_setArmor);
  lua_setglobal(L, "fh_set_armor");
  lua_pushcfunction(L, lua_setWeapon);
  lua_setglobal(L, "fh_set_weapon");
  lua_pushcfunction(L, lua_sendFile);
  lua_setglobal(L, "fh_send_file");
  lua_pushcfunction(L, lua_clrScreen);
  lua_setglobal(L, "fh_clr_screen"); 
}

void other_places_versions() {
  int i;

  for (i = 0; i < otherplaces_count; i++) {
    od_printf("%s (%s) [%s]\r\n", otherplaces[i]->name, otherplaces[i]->version,
              otherplaces[i]->author);
  }
}

void other_places_menu() {
  int i;
  char buffer[PATH_MAX];
  int done = 0;
  int selection;
  lua_State *L;

  while (!done) {
    od_clr_scr();

    od_printf("`bright white`Other Places`white`\r\n\r\n");

    for (i = 0; i < otherplaces_count; i++) {
      od_printf("`white`(`bright white`%d`white`) %s\r\n", i + 1,
                otherplaces[i]->name);
    }
    od_printf("`white`(`bright white`R`white`) Return to Town\r\n");

    od_printf("? ");
    od_input_str(buffer, 3, 32, 126);

    if (tolower(buffer[0]) == 'r') {
      done = 1;
    } else {
      selection = atoi(buffer);
      if (selection < 1 || selection > otherplaces_count) {
        od_printf("`bright red`I don't know where that is!`white`\r\n");
        od_printf("Press a key...");
        od_get_key(TRUE);
      } else {
        selection--;
        snprintf(buffer, sizeof buffer, "scripts%c%s", PATH_SEP,
                 otherplaces[selection]->luascript);

        L = luaL_newstate();
        luaL_openlibs(L);
        lua_push_cfunctions(L);
        if (luaL_dofile(L, buffer)) {
          od_printf("`bright red`%s`white`\r\n", lua_tostring(L, -1));
        }
        lua_close(L);
      }
    }
  }
}