#!/bin/sh

unamestr=`uname`

if [ ! -e odoors ]
then
    git clone https://gitlab.com/apamment/odoors.git
fi

cd odoors
if [ "$unamestr" == 'Linux' ]; then
   make
else
   gmake
fi

cd ..

gcc -c inih/ini.c -o inih/ini.o 
gcc -c main.c -o main.o -I./odoors/ `pkg-config --cflags lua-5.3`
gcc -c interbbs2.c -o interbbs2.o
gcc -c otherplaces.c -o otherplaces.o -I./odoors/ `pkg-config --cflags lua-5.3`
gcc -o ForHonour main.o otherplaces.o interbbs2.o inih/ini.o odoors/libs-`uname -s`/libODoors.a -lsqlite3 `pkg-config --libs lua-5.3`
