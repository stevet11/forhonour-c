#include <ctype.h>
#include <dirent.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include "interbbs2.h"

#ifdef _WIN32
#include <winsock2.h>
#else
#include <arpa/inet.h>
#endif // _MSC_VER

#define NUM_KEYWORDS 4

char *apszKeyWord[NUM_KEYWORDS] = {"LinkNodeNumber", "LinkFileOutbox",
                                   "LinkName", "GameID"};

void (*logger)(char *fmt, ...) = NULL;

tIBResult IBSend_Real(tIBInfo *pInfo, int pfrom, int pszDestNode, void *pBuffer,
                      uint32_t nBufferSize);

extern char *bad_path;
extern int delete_bad;

void IBSetLogger(void (*l)(char *fmt, ...)) { logger = l; }

int copyFile(const char *inf, const char *ouf)
{
    FILE *fptr1;
    FILE *fptr2;
    int count;

    char buffer[256];

    fptr1 = fopen(inf, "rb");
    if (!fptr1)
    {
        if (logger != NULL)
        {
            logger("Unable to open file for reading: %s", inf);
        }
        else
        {
            fprintf(stderr, "Unable to open file for reading: %s\n", inf);
        }
        return -1;
    }

    fptr2 = fopen(ouf, "wb");
    if (!fptr2)
    {
        if (logger != NULL)
        {
            logger("Unable to open file for writing: %s", ouf);
        }
        else
        {
            fprintf(stderr, "Unable to open file for writing: %s\n", ouf);
        }
        fclose(fptr1);
        return -1;
    }
    do
    {
        count = fread(buffer, 1, 256, fptr1);
        fwrite(buffer, 1, count, fptr2);
    } while (count == 256);

    fclose(fptr1);
    fclose(fptr2);

    return 0;
}

tIBResult ProcessFile(tIBInfo *pInfo, char *filename, void *pBuffer,
                      int nBufferSize)
{
    char version[6];
    FILE *fptr;
    memset(version, 0, 6);
    uint32_t destination;
    uint32_t memsize;
    int forward = 0;
    tIBResult result;
    uint32_t league;
    uint32_t gameid;
    int pfrom = 0;

    fptr = fopen(filename, "rb");
    if (!fptr)
    {
        if (logger != NULL)
        {
            logger("Error opening file %s", filename);
        }
        else
        {
            fprintf(stderr, "Error opening file %s\n", filename);
        }
        return eFileOpenError;
    }

    if (tolower(filename[strlen(filename) - 12]) >= 'a' &&
        tolower(filename[strlen(filename) - 12]) <= 'f')
    {
        pfrom = (int)((tolower(filename[strlen(filename) - 12]) - 'a' + 10) * 16);
    }
    else
    {
        pfrom = (int)((filename[strlen(filename) - 12] - '0') * 16);
    }

    if (tolower(filename[strlen(filename) - 11]) >= 'a' &&
        tolower(filename[strlen(filename) - 11]) <= 'f')
    {
        pfrom = pfrom + (tolower(filename[strlen(filename) - 11]) - 'a' + 10);
    }
    else
    {
        pfrom = pfrom + (filename[strlen(filename) - 11] - '0');
    }

    fread(version, 5, 1, fptr);

    fread(&league, sizeof(uint32_t), 1, fptr);
    league = ntohl(league);

    if (league != pInfo->league)
    {
        fclose(fptr);
        return eWrongLeague;
    }

    if (strncmp(version, VERSION, 5) != 0)
    {
        if (logger != NULL)
        {
            logger("Version Mismatch, is your game up to date? (Packet: %s vs Local: "
                   "%s) FROM Node: %d",
                   version, VERSION, pfrom);
        }
        else
        {
            fprintf(stderr,
                    "Version Mismatch, is your game up to date? (Packet: %s vs "
                    "Local: %s) FROM Node: %d\n",
                    version, VERSION, pfrom);
        }
        fclose(fptr);
        return eBadParameter;
    }

    fread(&destination, 1, sizeof(uint32_t), fptr);
    destination = ntohl(destination);

    if (destination != pInfo->myNode->nodeNumber)
    {
        forward = 1;
    }

    fread(&gameid, 1, sizeof(uint32_t), fptr);
    gameid = ntohl(gameid);

    if (pInfo->game_id == 0 || pInfo->game_id != gameid)
    {
        if (logger != NULL)
        {
            logger(
                "Game id %d does not equal packet game id %d (or is 0) FROM Node: %d",
                pInfo->game_id, gameid, pfrom);
        }
        else
        {
            fprintf(stderr,
                    "Game id %d does not equal packet game id %d (or is 0) FROM "
                    "Node: %d\n",
                    pInfo->game_id, gameid, pfrom);
        }
        fclose(fptr);
        return eWrongGameID;
    }

    fread(&memsize, sizeof(uint32_t), 1, fptr);
    memsize = ntohl(memsize);

    if (nBufferSize < memsize)
    {
        fclose(fptr);
        return eBadParameter;
    }
    fread(pBuffer, memsize, 1, fptr);

    if (forward)
    {
        result = IBSend_Real(pInfo, pfrom, destination, pBuffer, memsize);

        if (result == eSuccess)
        {
            fclose(fptr);
            unlink(filename);
            return eForwarded;
        }
        fclose(fptr);
        return result;
    }

    fclose(fptr);
    unlink(filename);

    return eSuccess;
}

tIBResult IBGet(tIBInfo *pInfo, void *pBuffer, uint32_t nBufferSize)
{
    DIR *dirp;
    struct dirent *dp;
    char filename[PATH_MAX];
    char filename_bad[PATH_MAX];
    tIBResult result;
    dirp = opendir(pInfo->myNode->filebox);
    if (!dirp)
    {
        return eMissingDir;
    }

    while ((dp = readdir(dirp)) != NULL)
    {
        if (strlen(dp->d_name) < 3)
            continue;
        if (strncmp(&dp->d_name[strlen(dp->d_name) - 3], FILEEXT, 3) == 0)
        {
            snprintf(filename, PATH_MAX, "%s%s%s", pInfo->myNode->filebox, PATH_SEP,
                     dp->d_name);
            result = ProcessFile(pInfo, filename, pBuffer, nBufferSize);
            if (result == eBadParameter)
            {
                if (!delete_bad)
                {
                    if (bad_path != NULL)
                    {
                        snprintf(filename_bad, PATH_MAX, "%s%s%s.BAD", bad_path, PATH_SEP,
                                 dp->d_name);
                    }
                    else
                    {
                        snprintf(filename_bad, PATH_MAX, "%s.BAD", filename);
                    }
                    copyFile(filename, filename_bad);
                }
                unlink(filename);
                rewinddir(dirp);
                continue;
            }
            else if (result == eWrongLeague)
            {
                continue;
            }
            else if (result == eWrongGameID)
            {
                if (!delete_bad)
                {
                    if (bad_path != NULL)
                    {
                        snprintf(filename_bad, PATH_MAX, "%s%s%s.OLD", bad_path, PATH_SEP,
                                 dp->d_name);
                    }
                    else
                    {
                        snprintf(filename_bad, PATH_MAX, "%s.OLD", filename);
                    }
                    copyFile(filename, filename_bad);
                }
                unlink(filename);
                rewinddir(dirp);
                continue;
            }
            closedir(dirp);
            return result;
        }
    }

    closedir(dirp);
    return eNoMoreMessages;
}

tIBResult IBSendAll(tIBInfo *pInfo, void *pBuffer, uint32_t nBufferSize)
{
    int i;
    int result;

    for (i = 0; i < pInfo->otherNodeCount; i++)
    {
        if (pInfo->otherNodes[i]->nodeNumber == pInfo->myNode->nodeNumber)
            continue;
        result =
            IBSend(pInfo, pInfo->otherNodes[i]->nodeNumber, pBuffer, nBufferSize);
        if (result != eSuccess)
        {
            return result;
        }
    }

    return eSuccess;
}

tIBResult IBSend(tIBInfo *pInfo, int pszDestNode, void *pBuffer,
                 uint32_t nBufferSize)
{
    return IBSend_Real(pInfo, pInfo->myNode->nodeNumber, pszDestNode, pBuffer,
                       nBufferSize);
}

tIBResult IBSend_Real(tIBInfo *pInfo, int pfrom, int pszDestNode, void *pBuffer,
                      uint32_t nBufferSize)
{
    int i;
    char filename[PATH_MAX];
    tOtherNode *dest = NULL;
    FILE *fptr;
    int packetno = 0;

    fptr = fopen("packetno.dat", "rb");
    if (fptr)
    {
        fread(&packetno, sizeof(int), 1, fptr);
        fclose(fptr);
    }

    for (i = 0; i < pInfo->otherNodeCount; i++)
    {
        if (pInfo->otherNodes[i]->nodeNumber == pszDestNode)
        {
            dest = pInfo->otherNodes[i];
            break;
        }
    }

    if (dest == NULL)
    {
        return eBadParameter;
    }

    if (packetno == 0x1000000)
    {
        packetno = 0;
    }

    snprintf(filename, PATH_MAX, "%s%s%02X%06X.%s", dest->filebox, PATH_SEP,
             pfrom, packetno, FILEEXT);
    packetno++;

    fptr = fopen(filename, "wb");
    if (!fptr)
    {
        return eFileOpenError;
    }

    uint32_t nwNbufferSize = htonl(nBufferSize);
    uint32_t leagueno = htonl(pInfo->league);
    uint32_t destn = htonl(pszDestNode);
    uint32_t gameid = htonl(pInfo->game_id);
    fwrite(VERSION, 5, 1, fptr);
    fwrite(&leagueno, sizeof(uint32_t), 1, fptr);
    fwrite(&destn, sizeof(uint32_t), 1, fptr);
    fwrite(&gameid, sizeof(uint32_t), 1, fptr);
    fwrite(&nwNbufferSize, sizeof(uint32_t), 1, fptr);
    fwrite(pBuffer, nBufferSize, 1, fptr);
    fclose(fptr);

    fptr = fopen("packetno.dat", "wb");
    if (!fptr)
    {
        if (logger != NULL)
        {
            logger("Unable to open packetno.dat");
        }
        else
        {
            fprintf(stderr, "Unable to open packetno.dat");
        }
    }
    else
    {
        fwrite(&packetno, sizeof(int), 1, fptr);
        fclose(fptr);
    }
    return eSuccess;
}

void ProcessConfigLine(int nKeyword, char *pszParameter, void *pCallbackData)
{
    tIBInfo *pInfo = (tIBInfo *)pCallbackData;
    tOtherNode **paNewNodeArray;

    switch (nKeyword)
    {
    case 0:
        if (pInfo->otherNodeCount == 0)
        {
            pInfo->otherNodes = (tOtherNode **)malloc(sizeof(tOtherNode *));
            if (pInfo->otherNodes == NULL)
            {
                break;
            }
        }
        else
        {
            if ((paNewNodeArray = (tOtherNode **)realloc(
                     pInfo->otherNodes,
                     sizeof(tOtherNode *) * (pInfo->otherNodeCount + 1))) == NULL)
            {
                break;
            }
            else
            {
                pInfo->otherNodes = paNewNodeArray;
            }
        }
        pInfo->otherNodes[pInfo->otherNodeCount] =
            (tOtherNode *)malloc(sizeof(tOtherNode));
        if (!pInfo->otherNodes[pInfo->otherNodeCount])
        {
            break;
        }

        pInfo->otherNodes[pInfo->otherNodeCount]->nodeNumber = atoi(pszParameter);

        strncpy(pInfo->otherNodes[pInfo->otherNodeCount]->filebox,
                pInfo->defaultFilebox, PATH_MAX);
        pInfo->otherNodes[pInfo->otherNodeCount]->filebox[PATH_MAX] = '\0';

        ++pInfo->otherNodeCount;
        break;

    case 1:
        if (pInfo->otherNodeCount != 0)
        {
            strncpy(pInfo->otherNodes[pInfo->otherNodeCount - 1]->filebox,
                    pszParameter, PATH_MAX);
            pInfo->otherNodes[pInfo->otherNodeCount - 1]->filebox[PATH_MAX] = '\0';
        }
        break;
    case 2:
        if (pInfo->otherNodeCount != 0)
        {
            strncpy(pInfo->otherNodes[pInfo->otherNodeCount - 1]->name, pszParameter,
                    SYSTEM_NAME_CHARS);
            pInfo->otherNodes[pInfo->otherNodeCount - 1]->name[SYSTEM_NAME_CHARS] =
                '\0';
        }
        break;
    case 3:
        pInfo->game_id = atoi(pszParameter);
        break;
    }
}

/* Configuration file reader settings */
#define CONFIG_LINE_SIZE 128
#define MAX_TOKEN_CHARS 32

tBool ProcessConfigFile(char *pszFileName, int nKeyWords, char **papszKeyWord,
                        void (*pfCallBack)(int, char *, void *),
                        void *pCallBackData)
{
    FILE *pfConfigFile;
    char szConfigLine[CONFIG_LINE_SIZE + 1];
    char *pcCurrentPos;
    unsigned int uCount;
    char szToken[MAX_TOKEN_CHARS + 1];
    int iKeyWord;

    /* Attempt to open configuration file */
    if ((pfConfigFile = fopen(pszFileName, "rt")) == NULL)
    {
        return (FALSE);
    }

    /* While not at end of file */
    while (!feof(pfConfigFile))
    {
        /* Get the next line */
        if (fgets(szConfigLine, CONFIG_LINE_SIZE + 1, pfConfigFile) == NULL)
            break;

        /* Ignore all of line after comments or CR/LF char */
        pcCurrentPos = (char *)szConfigLine;
        while (*pcCurrentPos)
        {
            if (*pcCurrentPos == '\n' || *pcCurrentPos == '\r' ||
                *pcCurrentPos == ';')
            {
                *pcCurrentPos = '\0';
                break;
            }
            ++pcCurrentPos;
        }

        /* Search for beginning of first token on line */
        pcCurrentPos = (char *)szConfigLine;
        while (*pcCurrentPos && isspace(*pcCurrentPos))
            ++pcCurrentPos;

        /* If no token was found, proceed to process the next line */
        if (!*pcCurrentPos)
            continue;

        /* Get first token from line */
        uCount = 0;
        while (*pcCurrentPos && !isspace(*pcCurrentPos))
        {
            if (uCount < MAX_TOKEN_CHARS)
                szToken[uCount++] = *pcCurrentPos;
            ++pcCurrentPos;
        }
        if (uCount <= MAX_TOKEN_CHARS)
            szToken[uCount] = '\0';
        else
            szToken[MAX_TOKEN_CHARS] = '\0';

        /* Find beginning of configuration option parameters */
        while (*pcCurrentPos && isspace(*pcCurrentPos))
            ++pcCurrentPos;

        /* Trim trailing spaces from setting string */
        for (uCount = strlen(pcCurrentPos) - 1; uCount > 0; --uCount)
        {
            if (isspace(pcCurrentPos[uCount]))
            {
                pcCurrentPos[uCount] = '\0';
            }
            else
            {
                break;
            }
        }

        /* Loop through list of keywords */
        for (iKeyWord = 0; iKeyWord < nKeyWords; ++iKeyWord)
        {
            /* If keyword matches */
            if (strcasecmp(szToken, papszKeyWord[iKeyWord]) == 0)
            {
                /* Call keyword processing callback function */
                (*pfCallBack)(iKeyWord, pcCurrentPos, pCallBackData);
            }
        }
    }

    /* Close the configuration file */
    fclose(pfConfigFile);

    /* Return with success */
    return (TRUE);
}

tIBResult IBReadConfig(tIBInfo *pInfo, char *pszConfigFile)
{
    /* Set default values for pInfo settings */
    pInfo->otherNodeCount = 0;
    pInfo->otherNodes = NULL;

    /* Process configuration file */
    if (!ProcessConfigFile(pszConfigFile, NUM_KEYWORDS, apszKeyWord,
                           ProcessConfigLine, (void *)pInfo))
    {
        return (eFileOpenError);
    }

    /* else */
    return (eSuccess);
}
